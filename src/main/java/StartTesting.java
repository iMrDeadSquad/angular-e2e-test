import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.NgWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class StartTesting {

    public void StartTest() throws InterruptedException {


        System.setProperty("webdriver.chrome.driver", "C:\\Users\\niekm\\Downloads\\chromedriver_win32\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("http://192.168.44.215/home");
        driver.manage().window().maximize();

        NgWebDriver ngDriver = new NgWebDriver((JavascriptExecutor) driver);

        ngDriver.waitForAngularRequestsToFinish();
        Thread.sleep(2000);

        CheckIfCanAccessSupplierWithoutJWT(driver);
        Login(driver);
        SearchSupplier(driver);
        AddSupplier(driver);
        AddNoteByNewSupplier(driver);
        AddCustomer(driver);
//        AddEmployee(driver);
//        EditEmployee(driver);
        EditSupplier(driver);
    }

    public void CheckIfCanAccessSupplierWithoutJWT(WebDriver driver) throws InterruptedException {
        driver.findElement(By.id("suppliers")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("customers")).click();
        Thread.sleep(2000);
    }

    public void Login(WebDriver driver) throws InterruptedException {
        driver.findElement(By.id("login")).click();
        WebElement username = driver.findElement(By.id("username"));
        WebElement password = driver.findElement(By.id("password"));

        Thread.sleep(1000);
        username.sendKeys("admin");
        Thread.sleep(2000);
        password.sendKeys("test1234");
        Thread.sleep(2000);
        driver.findElement(ByAngular.buttonText("Login")).click();

        password.clear();
        username.clear();
        Thread.sleep(2000);


        username.sendKeys("admin");
        Thread.sleep(1000);
        password.sendKeys("test123");
        Thread.sleep(1000);
        driver.findElement(ByAngular.buttonText("Login")).click();
        Thread.sleep(1000);
    }

    public void SearchSupplier(WebDriver driver) throws InterruptedException {
        driver.findElement(By.id("suppliers")).click();
        Thread.sleep(1000);

        WebElement searchbar = driver.findElement(By.id("searchCompanyName"));
        Thread.sleep(500);
        searchbar.sendKeys("Fontys");
        Thread.sleep(1000);
        driver.findElement(By.id("searchButton")).click();
        Thread.sleep(2000);

    }

    public void AddSupplier(WebDriver driver) throws InterruptedException {
        driver.findElement(By.id("suppliers")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("addsupplier")).click();

        WebElement companyName = driver.findElement(By.id("companyName"));
        WebElement contact = driver.findElement(By.id("contact"));
        WebElement street = driver.findElement(By.id("street"));
        WebElement houseNumber = driver.findElement(By.id("houseNumber"));
        WebElement addition = driver.findElement(By.id("addition"));
        WebElement postalCode = driver.findElement(By.id("postalCode"));
        WebElement place = driver.findElement(By.id("place"));
        WebElement email = driver.findElement(By.id("email"));
        WebElement phoneNumber = driver.findElement(By.id("phoneNumber"));

        Thread.sleep(500);
        companyName.sendKeys("Fontys");
        Thread.sleep(500);
        contact.sendKeys("");
        Thread.sleep(500);
        street.sendKeys("Professorlaan");
        Thread.sleep(500);
        houseNumber.sendKeys("5");
        Thread.sleep(500);
        addition.sendKeys("");
        Thread.sleep(500);
        postalCode.sendKeys("5012TRR");
        Thread.sleep(500);
        place.sendKeys("Tilburg");
        Thread.sleep(500);
        email.sendKeys("fontys");
        Thread.sleep(500);
        phoneNumber.sendKeys("0311374");
        Thread.sleep(500);


        driver.findElement(By.id("save")).click();
        Thread.sleep(2000);

        Thread.sleep(500);
        contact.clear();
        contact.sendKeys("Niek Moor");
        Thread.sleep(500);
        postalCode.clear();
        postalCode.sendKeys("5012TR");
        Thread.sleep(500);
        email.clear();
        email.sendKeys("fontys@fontys.nl");
        Thread.sleep(500);
        phoneNumber.clear();
        phoneNumber.sendKeys("03156231374");
        Thread.sleep(500);


        driver.findElement(By.id("save")).click();

        Thread.sleep(1000);
    }
    public void AddNoteByNewSupplier(WebDriver driver) throws InterruptedException {
        driver.findElement(By.id("suppliers")).click();
        Thread.sleep(2000);
        List<WebElement> td = driver.findElements(By.id("list"));;
        td.get(4).click();
        Thread.sleep(1000);
        WebElement content = driver.findElement(By.id("content"));
        content.sendKeys("This is a test note");
        Thread.sleep(1000);
        driver.findElement(By.id("Submit")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("home")).click();
        Thread.sleep(2000);

    }

    public void AddCustomer (WebDriver driver) throws InterruptedException {
        driver.findElement(By.id("customers")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("addcustomers")).click();

        WebElement companyName = driver.findElement(By.id("companyName"));
        WebElement firstName = driver.findElement(By.id("firstName"));
        WebElement lastName = driver.findElement(By.id("lastName"));
        WebElement street = driver.findElement(By.id("street"));
        WebElement houseNumber = driver.findElement(By.id("houseNumber"));
        WebElement addition = driver.findElement(By.id("addition"));
        WebElement postalCode = driver.findElement(By.id("postalCode"));
        WebElement place = driver.findElement(By.id("place"));
        WebElement email = driver.findElement(By.id("email"));
        WebElement phoneNumber = driver.findElement(By.id("phoneNumber"));
        WebElement status = driver.findElement(By.id("status"));
        WebElement kvk = driver.findElement(By.id("kvk"));
        WebElement btw = driver.findElement(By.id("btw"));

        Thread.sleep(500);
        companyName.sendKeys("Fontys");
        commenData(firstName, lastName, street, houseNumber, addition, postalCode, place, email, phoneNumber);
        Select Opt = new Select(driver.findElement(By.id("status")));
        Opt.selectByValue("active");
        Thread.sleep(500);
        kvk.sendKeys("kvk643295678");
        Thread.sleep(500);
        btw.sendKeys("NL848743HD8D");
        Thread.sleep(500);

        driver.findElement(By.id("save")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("home")).click();
        Thread.sleep(2000);
    }

//    public void AddEmployee (WebDriver driver) throws InterruptedException {
//        driver.findElement(By.id("customers")).click();
//        Thread.sleep(1000);
//        driver.findElement(By.id("listActive")).click();
//        Thread.sleep(1000);
//        driver.findElement(By.id("more")).click();
//        Thread.sleep(1000);
//        driver.findElement(By.id("employees")).click();
//        Thread.sleep(1000);
//        driver.findElement(By.id("addEmployee")).click();
//        Thread.sleep(1000);
//
//        WebElement firstName = driver.findElement(By.id("firstName"));
//        WebElement lastName = driver.findElement(By.id("lastName"));
//        WebElement street = driver.findElement(By.id("street"));
//        WebElement houseNumber = driver.findElement(By.id("houseNumber"));
//        WebElement addition = driver.findElement(By.id("addition"));
//        WebElement postalCode = driver.findElement(By.id("postalCode"));
//        WebElement place = driver.findElement(By.id("place"));
//        WebElement email = driver.findElement(By.id("email"));
//        WebElement phoneNumber = driver.findElement(By.id("phoneNumber"));
//
//        commenData(firstName, lastName, street, houseNumber, addition, postalCode, place, email, phoneNumber);
//        driver.findElement(By.id("save")).click();
//        Thread.sleep(80);
//        driver.navigate().refresh();
//    }

//    public void EditEmployee(WebDriver driver) throws InterruptedException{
//
//        driver.findElement(By.id("customers")).click();
//        Thread.sleep(1000);
//        driver.findElement(By.id("listActive")).click();
//        Thread.sleep(1000);
//        driver.findElement(By.id("more")).click();
//        Thread.sleep(1000);
//        driver.findElement(By.id("employees")).click();
//        Thread.sleep(1000);
//        List<WebElement> td = driver.findElements(By.id("list"));;
//        td.get(1).click();
//        Thread.sleep(1000);
//        WebElement email = driver.findElement(By.id("email"));
//        WebElement lastName = driver.findElement(By.id("lastName"));
//
//        email.clear();
//        Thread.sleep(1000);
//        lastName.clear();
//        Thread.sleep(1000);
//        lastName.sendKeys("Janssen");
//        Thread.sleep(1000);
//        email.sendKeys("fontys@student.fontys.nl");
//        Thread.sleep(2000);
//        driver.findElement(By.id("save")).click();
//        Thread.sleep(1000);
//        driver.navigate().refresh();
//        Thread.sleep(1000);
//        List<WebElement> td1 = driver.findElements(By.id("list"));;
//        td1.get(1).click();
//        Thread.sleep(1000);
//        WebElement email2 = driver.findElement(By.id("email"));
//        WebElement lastName2 = driver.findElement(By.id("lastName"));
//        email2.clear();
//        Thread.sleep(1000);
//        lastName2.clear();
//        Thread.sleep(1000);
//        lastName2.sendKeys("Moor");
//        Thread.sleep(1000);
//        email2.sendKeys("fontys@fontys.nl");
//        Thread.sleep(1000);
//        driver.findElement(By.id("save")).click();
//        Thread.sleep(1000);
//    }

    public void EditSupplier(WebDriver driver) throws InterruptedException{
        driver.findElement(By.id("suppliers")).click();
        Thread.sleep(1000);
        List<WebElement> td1 = driver.findElements(By.id("list"));;
        td1.get(0).click();

        WebElement contact = driver.findElement(By.id("contact"));
        WebElement street = driver.findElement(By.id("street"));
        WebElement houseNumber = driver.findElement(By.id("houseNumber"));
        WebElement postalCode = driver.findElement(By.id("postalCode"));
        WebElement place = driver.findElement(By.id("place"));

        Thread.sleep(1000);
        contact.clear();
        Thread.sleep(1000);
        street.clear();
        Thread.sleep(1000);
        houseNumber.clear();
        Thread.sleep(1000);
        postalCode.clear();
        Thread.sleep(1000);
        place.clear();
        Thread.sleep(1000);
        contact.sendKeys("Niek Janssen");
        Thread.sleep(1000);
        street.sendKeys("Van Evertstraat");
        Thread.sleep(1000);
        houseNumber.sendKeys("27");
        Thread.sleep(1000);
        postalCode.sendKeys("4925RV");
        Thread.sleep(1000);
        place.sendKeys("Groningen");
        Thread.sleep(1000);

        driver.findElement(By.id("save")).click();
        Thread.sleep(1000);
        driver.navigate().back();

        WebElement contact2 = driver.findElement(By.id("contact"));
        WebElement street2 = driver.findElement(By.id("street"));
        WebElement houseNumber2 = driver.findElement(By.id("houseNumber"));
        WebElement postalCode2 = driver.findElement(By.id("postalCode"));
        WebElement place2 = driver.findElement(By.id("place"));

        Thread.sleep(1000);
        contact2.clear();
        Thread.sleep(1000);
        street2.clear();
        Thread.sleep(1000);
        houseNumber2.clear();
        Thread.sleep(1000);
        postalCode2.clear();
        Thread.sleep(1000);
        place2.clear();

        Thread.sleep(1000);
        contact2.sendKeys("Niek Moor");
        Thread.sleep(1000);
        street2.sendKeys("Professorlaan");
        Thread.sleep(1000);
        houseNumber2.sendKeys("5");
        Thread.sleep(1000);
        postalCode2.sendKeys("5012TR");
        Thread.sleep(1000);
        place2.sendKeys("Tilburg");
        Thread.sleep(1000);
        driver.findElement(By.id("save")).click();

        System.out.println("tests succesfull, window wil close within 2 seconds");
        Thread.sleep(2000);
        driver.close();

    }

    private void commenData(WebElement firstName, WebElement lastName, WebElement street, WebElement houseNumber, WebElement addition, WebElement postalCode, WebElement place, WebElement email, WebElement phoneNumber) throws InterruptedException {
        Thread.sleep(500);
        firstName.sendKeys("Niek");
        Thread.sleep(500);
        lastName.sendKeys("Moor");
        Thread.sleep(500);
        street.sendKeys("Professorlaan");
        Thread.sleep(500);
        houseNumber.sendKeys("5");
        Thread.sleep(500);
        addition.sendKeys("");
        Thread.sleep(500);
        postalCode.sendKeys("5012TR");
        Thread.sleep(500);
        place.sendKeys("Tilburg");
        Thread.sleep(500);
        email.sendKeys("fontys@fontys.nl");
        Thread.sleep(500);
        phoneNumber.sendKeys("03156231374");
        Thread.sleep(500);
    }
}


